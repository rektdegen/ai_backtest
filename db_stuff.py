"""mysql actions"""

import MySQLdb
from parameters import EVENT_TABLE_NAME, PRICE_COUNT, LAST_N_LEN_MULTIPLIER


def new_event_table():
    """this creates a new event table to be populated with signature events"""

    # Open database connection
    db_instance = MySQLdb.connect("localhost", "user", "", "mysql")

    # prepare a cursor object using cursor() method
    cursor = db_instance.cursor()

# Drop table if it already exist using execute() method.
    cursor.execute("DROP TABLE IF EXISTS %s " % EVENT_TABLE_NAME)

# define table
    sql = """CREATE TABLE %s (
            SIGNATURE  CHAR(13) NOT NULL,
            PRICE_DATA_INDEX  INT(10),
            LAST_N_LEN INT,
            LAST_STANDARD_DEVIATION FLOAT(7,5)
            """ % EVENT_TABLE_NAME

    for last_n_range in range((PRICE_COUNT + 1) * LAST_N_LEN_MULTIPLIER):
        sql += (", PRICE_%s FLOAT(8,5)" % (last_n_range))
    sql += ")"
    # disconnect from server
    cursor.execute(sql)
    db_instance.close()
    print("new table initiated")
