"""algo manage sig and stats
this is sample algo, add your own features here"""
from parameters import LAST_N_LEN


class SigGen():
    """this returns a signature for a given time series"""

    def __init__(self):
        """defines generation variables"""
        self.sigs_d = {'rsi_1': None,  # each variable ranges from 0-4 for each quintile range
                       'ma_1': None,  # this allows for 4^5 or 024 possible signatures
                       'stoch_rsa_1': None,
                       'stoch_ma_1': None}
        self.last_n = LAST_N_LEN
        self.sig = None

    def generate(self, market_d=None, last_n=LAST_N_LEN):
        """runs generation code"""
        self.sigs_d = {}
        self.last_n = last_n
        if len(market_d) == last_n:
            self.calculate(market_d)
            self.compile_sigs()
            self.define_sig()
        else:
            self.sig = None
        return self.sig

    def calculate(self, market_d):
        """calculate variables"""
        pass

    def compile_sigs(self):
        """compile data into signature list"""
        pass

    def define_sig(self):
        """defines sig for output"""
        pass
