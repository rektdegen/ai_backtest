"""manage database"""
import MySQLdb
from parameters import DB_NAME, DB_USERNAME, DB_PASSWORD


class DataBaseManager():
    """create new db, tables
    add entries to tables"""
    def __init__(self, stats_dict, bests_dict, positions_dict):

        self.tables = {'stats': stats_dict,
                       'bests': bests_dict}
        # todo add positions table to database

        self.db = None
        self.cursor = None

    def write_to_db(self):
        """commit changes to database"""
        db = MySQLdb.connect(host="localhost", user=DB_USERNAME, passwd=DB_PASSWORD)
        cursor = db.cursor()
        cursor.execute('CREATE DATABASE IF NOT EXISTS %s' % DB_NAME)
        db.close()
        print('')
        print('initiating database write')
        print('')
        self.connect_to_db()
        self.new_tables()
        self.populate_tables()
        self.db.commit()
        self.db.close()

    def db_close(self):
        """close connection"""
        self.db.close()

    def connect_to_db(self):
        """conect to database"""
        self.db = MySQLdb.connect("localhost",
                                  DB_USERNAME,
                                  DB_PASSWORD,
                                  DB_NAME)
        self.cursor = self.db.cursor()

    def new_tables(self):
        """create tables"""
        tables_dict = {'events': """SIG CHAR(13) NOT NULL,
                                      EVENT_TICK  TEXT,
                                      LAST_N_LEN INT,
                                      FUTURE_TICKS TEXT""",
                       'stats': """SIG CHAR(13) NOT NULL,
                                      COUNT INT,
                                      CLOSE_ON INT,
                                      WIN_RATE FLOAT(7,5),
                                      MMP FLOAT(7,5),
                                      AVERAGE_GAIN FLOAT(7,5),
                                      CONFIDENCE FLOAT(7,5),
                                      HALF_KELLY FLOAT(7,5),
                                      STD FLOAT(7,5),
                                      RAW_STD FLOAT(7,5),
                                      LAST_N INT,
                                      SHARPE FLOAT(7,5)""",
                       'bests': """SIG CHAR(13) NOT NULL,
                                      COUNT INT,
                                      CLOSE_ON INT,
                                      WIN_RATE FLOAT(7,5),
                                      MMP FLOAT(7,5),
                                      AVERAGE_GAIN FLOAT(7,5),
                                      CONFIDENCE FLOAT(7,5),
                                      HALF_KELLY FLOAT(7,5),
                                      STD FLOAT(7,5),
                                      RAW_STD FLOAT(7,5),
                                      LAST_N INT,
                                      SHARPE FLOAT(7,5)""",
                       'trades': """SIG CHAR(13) NOT NULL,
                                      COUNT INT,
                                      CLOSE_ON INT,
                                      WIN_RATE FLOAT(7,5),
                                      MMP FLOAT(7,5),
                                      AVERAGE_GAIN FLOAT(7,5),
                                      CONFIDENCE FLOAT(7,5),
                                      HALF_KELLY FLOAT(7,5),
                                      RAW_STD FLOAT(7,5),
                                      LAST_N INT,
                                      SHARPE FLOAT(7,5),
                                      ENTRY TEXT,
                                      LAST_TICK TEXT,
                                      CLOSE TEXT,
                                      PNL FLOAT(30,5),
                                      BALANCE_ADD FLOAT(30,5),
                                      BALANCE FLOAT(30,5),
                                      CONTRACTS INT,
                                      LEVERAGE FLOAT(7,5),
                                      FEES FLOAT(30,5),
                                      LIVE TINYINT(1)"""}

        for table in tables_dict:
            self.cursor.execute("DROP TABLE IF EXISTS %s" % table)
            sql_string = """CREATE TABLE %s (%s)""" % (table, tables_dict[table])
            self.cursor.execute(sql_string)

    def populate_tables(self):
        """populate stats table"""
        for table in self.tables:
            for row in self.tables[table]:
                insert_into = "INSERT INTO %s(" % table
                values = "VALUES("
                for value in row:
                    try:
                        if isinstance(row[value], str) is str:
                            row[value] = "%s" % row[value]
                        insert_into += "%s, " % value
                        values += "%s, " % str(row[value])
                    except TypeError:
                        pass
                insert_into = insert_into[:-2] + ") "
                if values != "INSERT INTO best) VALUE)" and values != "INSERT INTO stats) VALUE)":
                    values = values[:-2] + ")"
                    sql_string = insert_into + values
                    self.cursor.execute(sql_string)
                else:
                    print(sql_string, table)

    def get_sig_info(self, sig):
        """pull sig data from database"""
        sql = "SELECT * FROM bests \
            WHERE SIG = %s and LAST_N = %s" % (sig['sig'], sig['last_n'])
        self.cursor.execute(sql)
        results = self.cursor.fetchall()
        sig_info_dict = {}
        if results:
            value_list = ('sig',
                          'count',
                          'close_on',
                          'win_rate',
                          'mmp',
                          'average_gain',
                          'confidence',
                          'half_kelly',
                          'std',
                          'raw_std',
                          'last_n',
                          'sharpe')
            for ind, value in enumerate(value_list):
                sig_info_dict[value] = results[0][ind]
        return sig_info_dict
