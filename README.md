# SIMPLE AI BACKTEST FRAMEWORK

This AI framework allows for analysis of any arbitrary set of feature with any historical data over multiple time parameters. 

In this published version there is a sample strategy set and documentation of writing and testing your own. - TODO

### Prerequisites

python3

numpy

matplotlib

python3-mysql

mysql

## Input

Data for input must be a file with following format:

```yearmonthday hourminute;open;high;low;close;volume```

```20180711 0553;6311.98;6349.00;6285.00;6313.65;501.16```

A sample [data file](backtest_data_1000.cvs) has been provided.

## Tick Data

Data for forex and popular market indices: - [Histdata.com](http://www.histdata.com/download-free-forex-data/?/ascii/1-minute-bar-quotes)

Data for bitcoin: - [Bitcoincharts.com](https://api.bitcoincharts.com/v1/csv/)

## Data Parsing

Data from bitcoincharts.com needs to be parsed before it can be processed by this application.

Use the [Smart Tick Parser](/scripts/cvs_parser_smart_01.py)

```./cvs_parser_smart_01.py <raw_data.cvs> <data_file_name>```

```data_file_name``` may have excessive length and include early exchange data with low volume tick data.

You may need to truncate the data:

```tail -n <new_len> <data_file_name> > <new_data_file_file_name>```

## Running the Backtest

To test the default strategy with sample data, run: ```./lambo_backtest.py```

To run the test on data with another name/directory run: ```./lambo_backtest <dir/data_file_name>```

## Output

This tradebot has several outputs.

1) Print trade data to console.

2) Write trained model to database.

3) Save various performance charts to local directory.

See [/results](/results) for output samples.

## Deployment

To run trained model on live data run ```./live_sig.py```

## Authors

* **Rekt Degen** - [Blog and Research](https://rektdegen.wordpress.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
