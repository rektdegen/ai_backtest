"""generate charts"""
import matplotlib.pyplot as plt
import numpy as np


class Charts():
    """this class manages chart generation"""
    def __init__(self, tick_data_class, positions):
        self.tick_data_class = tick_data_class
        self.positions = positions

    def gen(self):
        """execute chart generation functions"""
        self.pnl_chart()
        self.positions_analysiss()

    def pnl_chart(self):
        """chart pnl over price"""
        dates = self.tick_data_class.daily_tick['time']
        pnls = self.tick_data_class.daily_tick['pnl']
        prices = self.tick_data_class.daily_tick['price']

        # plot pnl
        ax1 = plt.gca()
        ax_1_plot = ax1.plot(dates, pnls, color='black', linewidth=1, label='btc_balance')
        ax1.set_xlabel("time")
        ax1.set_ylabel("pnl")

        # plot daily close
        ax2 = ax1.twinx()
        ax2.set_yscale('log')
        ax2.set_ylabel("daily_close")
        ax_2_plot = ax2.plot(dates, prices, color='blue', linewidth=1, label='daily_close')

        # add legend
        plots = ax_1_plot + ax_2_plot
        labels = [l.get_label() for l in plots]
        ax1.legend(plots, labels, loc=8)

        plt.title('lambobot, self-learning backtest pnl')
        plt.grid(True)
        plt.savefig("simple_lambobot.png")
        ax1.set_yscale('log')
        plt.savefig("simple_lambobot_log.png")
        plt.figure()

    def positions_analysiss(self):
        """create several charts with different x axis values"""
        x_labels = ['count',
                    'mmp',
                    'confidence',
                    'average_gain',
                    'win_rate',
                    'raw_std',
                    'sharpe',
                    'close_on',
                    'last_n']

        for label in x_labels:
            self.positions_analysis(label)

    def positions_analysis(self, x_label):
        """for each x_label get all positions with that label
        compile list of means from the label and plot it"""
        means_list = []
        xlable_list = []

        pnl_list = []
        amp_list = []
        mmp_list = []

        self.positions.sort(key=lambda x: x.signal[x_label])
        x_quantity_list = []
        for position in self.positions:
            if not position.signal[x_label] in x_quantity_list:
                x_quantity_list.append(position.signal[x_label])
        for x_quantity in x_quantity_list:
            x_iteration = [iteration for iteration in self.positions
                           if iteration.signal[x_label] == x_quantity]

            pnl = float(np.mean([abs((pos_pnl.close[0] / pos_pnl.entry[0]) - 1)
                                 for pos_pnl in x_iteration]))
            amp = abs(float(np.mean([position.signal['average_gain'] for position in x_iteration])))
            mmp = abs(float(np.mean([position.signal['mmp'] for position in x_iteration])))

            pnl_list.append(pnl)
            amp_list.append(amp)
            mmp_list.append(mmp)
            means_list.append(pnl - mmp)
            xlable_list.append(x_quantity)
        print(len(means_list), len(xlable_list), len(pnl_list))
        print()

        plt.plot(xlable_list, pnl_list, label='pnl')
        plt.plot(xlable_list, amp_list, label='amp')
        plt.plot(xlable_list, mmp_list, label='mmp')
        plt.xlabel(x_label)
        plt.ylabel("performance over minimum")
        plt.legend()
        plt.title(x_label + ' POM lambobot')
        plt.savefig("pom_" + x_label + ".png")
        plt.figure()

    def sharpe(self):
        """chart sharpe ratio with pnl and daily close TODO"""
        pass
