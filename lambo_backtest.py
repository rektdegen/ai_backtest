#!/usr/bin/python3
"""
this script uses simulates real out of band trading
it uses the event data to build and update an in memory database
then it uses that database to open trades
"""
import sys
import datetime
import numpy as np
from parameters import DEFAULT_CVS_FILE, INITAL_BALANCE, WAIT_THRESHOLD
from data_containers import TickData
from algo import LamboBot
from trader import Position
from gen_charts import Charts
from algo_database import DataBaseManager

class BackTestr():
    """this script intialized the backtester"""
    def __init__(self, cvs_file):

        self.balance = INITAL_BALANCE
        self.tick_data_class = TickData(cvs_file)
        self.live_algo = LamboBot()
        self.position = False
        self.last_positions = []
        self.charts = Charts(self.tick_data_class, self.last_positions)

        print('initialized')

    def run_backtest(self):
        """run the bot"""
        self.iterate_ticks()
        self.save_to_db()
        self.charts.gen()

    def iterate_ticks(self):
        """iterate over ticks"""
        init_time = datetime.datetime.now()
        for tick_index, tick in enumerate(self.tick_data_class.price_data):
            if tick_index % self.tick_data_class.percent_tick == 0:
                print(tick_index / self.tick_data_class.percent_tick,
                      '%',
                      str(datetime.datetime.now() - init_time))

            action = self.live_algo.new_tick(tick, self.position)

            if self.position:
                if self.position.next_tick(tick, action):   # this position was closed if returns
                                                            # anything so now move it to old pile
                    self.last_positions.append(self.position)
                    self.position = False

            if action and not self.position and (tick_index > WAIT_THRESHOLD):
                self.position = Position(action, tick, self.balance_add, self.balance)
        # save daily pnl.
            if tick[1].hour == 0:
                self.tick_data_class.daily_add(tick, self.balance)

        # close last position
        if self.position:
            self.position.close_position()
            self.last_positions.append(self.position)
        self.print_stats()
        print("backtest_time: " + str(datetime.datetime.now() - init_time))

    def print_stats(self):
        """after algo is finished running
        print performance info"""
        print('--------------')
        long_winners = 0
        short_winners = 0
        longs = 0
        shorts = 0
        long_gains = []
        short_gains = []
        print('btc_return:', ((self.balance / INITAL_BALANCE) - 1) * 100, "%")
        for close_position in self.last_positions:
            if close_position.contracts > 0:
                long_gains.append(-((close_position.entry[0] / close_position.close[0]) - 1))
                longs += 1
                if close_position.pnl > 0:
                    long_winners += 1
            else:
                short_gains.append(((close_position.entry[0] / close_position.close[0]) - 1))
                shorts += 1
                if close_position.pnl > 0:
                    short_winners += 1
        print('mean leverage', np.mean([position.leverage for position in self.last_positions]))
        print('end_balance:', self.balance)
        print('longs/shorts:', longs, shorts)
        print('total_trades', len(self.last_positions))
        print('return_per_trade:', sum(long_gains + short_gains) / len(self.last_positions))
        print('cum_gains long/short', sum(long_gains), sum(short_gains))
        if longs:
            print('winrate long:', long_winners / longs)
        if shorts:
            print('winrate short:', short_winners / shorts)

        print('win_rate:', (long_winners + short_winners) / len(self.last_positions))

    def balance_add(self, pnl):
        """add balance"""
        self.balance += pnl

    def save_to_db(self):
        """write patterns and trades to database"""
        db_manager = DataBaseManager(self.live_algo.stats_manager.stats_list,
                                     self.live_algo.stats_manager.best_trades.values(),
                                     self.last_positions)
        db_manager.write_to_db()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        CVS_FILE = sys.argv[1]
    else:
        CVS_FILE = DEFAULT_CVS_FILE
    BACKTEST = BackTestr(CVS_FILE)
    BACKTEST.run_backtest()
