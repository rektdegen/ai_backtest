"""here we simulate trading"""
from parameters import FEES, SLIPPAGE, PNL_CALC
from algo_position import ExitRules


class Position:
    """this is a position object"""
    def __init__(self, signal, tick, balance_add, balance):
        self.signal = signal
        for sig in signal:
            print(sig, signal[sig])
        self.entry = tick
        self.last_tick = self.entry
        self.close = False
        self.pnl = None
        self.balance_add = balance_add
        self.balance = balance
        self.contracts = 0
        self.live = False
        if signal['side'] == 'buy':
            self.contracts = int((signal['half_kelly'] - 1) * self.balance * self.entry[0])
            self.leverage = abs(signal['half_kelly'] - 1)
        else:
            self.contracts = int((-signal['half_kelly'] - 1) * self.balance * self.entry[0])
            self.leverage = abs(-signal['half_kelly'] - 1)
        self.fees = FEES * (abs(self.contracts) / self.entry[0])
        self.exit_rules = ExitRules(signal, tick)

    def next_tick(self, tick, algo_data, closed=False):
        """check new tick"""
        # check reversal
        closed = False
        if algo_data:
            if algo_data["side"] == "buy" and self.contracts < 0:
                self.close_position(tick)
                closed = True
            elif algo_data["side"] == "sell" and self.contracts > 0:
                self.close_position(tick)
                closed = True
        if not closed:
            # check exit conditions
            for exit_rule in self.exit_rules.rule_list:
                close_price = exit_rule(self.contracts, self.last_tick, tick)
                if close_price:
                    self.close_position(self.last_tick, close_price)
                    closed = True
                    break
        self.last_tick = tick
        return closed

    def close_position(self, tick=False, close_price=False):
        """send close data, return pnl"""
        if close_price:
            self.close = [close_price, tick[1]]
        elif tick:
            self.close = list(tick)
        else:
            self.close = list(self.last_tick)
        #calculate slippage
        if self.contracts > 0:
            self.close[0] -= SLIPPAGE
        else:
            self.close[0] += SLIPPAGE
        #add fees
        self.fees += FEES * (abs(self.contracts) / self.close[0])
        #calc pnl
        if PNL_CALC == "derivatives":
            # for bitmex and derebit
            pnl_base = ((1 / self.entry[0]) - (1 / self.close[0]))
            self.pnl = (pnl_base) * self.contracts
        elif PNL_CALC == "classic":
            # spot exchanges
            self.pnl = ((self.close[0] / self.entry[0]) - 1) * self.leverage
        #add balance
        self.balance_add(self.pnl - self.fees)

        print('pnl', self.pnl, 'balance', self.balance + self.pnl - self.fees, self.signal['side'])
        print('fees', self.fees)
        print('--------------')
        return self.pnl
