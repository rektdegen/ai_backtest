"""manage various dataframes
        1) event_dict
        2) signature_dict
        3) open_trade_dict
"""

import re
import datetime
from operator import itemgetter
from decimal import Decimal
import os.path
from math import sqrt
from statistics import stdev, mean
import numpy

from parameters import Z_VALUE, SIM_FEES, MAX_KELLY
from parameters import STD_MULTIPLIER, PRICE_COUNT, RETRAIN_N
from parameters import SL_L, BREAK_L


class SigEventsManager():
    """
    this class handles event data
        add_event(event_object) # adds event to event_signature_dict for signature_name
        get_events(signature) # pulls event array for signature
        this has to be updated every tick
    """
    def __init__(self):
        self.event_signature_dict = {}
        self.current_events = []

    def manage_data(self, tick, signature=False):
        """manage new tick input"""
        new_event = self.update_currents(tick[0])
        if signature:
            self.current_events.append({'signature': signature['sig'],
                                        'event_tick': tick,
                                        'last_n': signature['last_n'],
                                        'future_ticks': [Decimal(0)]})
        return new_event

    def add_event(self, event):
        """adds event to event_dict"""
        if event['signature'] in self.event_signature_dict:
            self.event_signature_dict[event['signature']].append(event)
        else:
            self.event_signature_dict[event['signature']] = [event]

    def update_currents(self, tick):
        """update live patterns with next tick"""
        new_event = False
        for ind, event in enumerate(self.current_events):
            new_tick = (tick / event['event_tick'][0]) - 1
            event['future_ticks'].append(new_tick)
            if len(event['future_ticks']) > event['last_n'] * PRICE_COUNT:
                # future len + 1 because intial tick is 0
                self.add_event(event)
                new_event = event
                del self.current_events[ind]
        return new_event

    def get_events(self, sig, last_n):
        """return events with last_n"""
        return_value = None
        if sig == 'all':
            return_value = [event for event in self.event_signature_dict
                            if event['last_n'] == last_n]
        else:
            return_value = [event for event in self.event_signature_dict[sig]
                            if event['last_n'] == last_n]
        return return_value

    def prune_events(self, event):
        """compact event list to reduce memory demand"""
        event_list = [(i, e) for i, e in enumerate(self.event_signature_dict[event['signature']])
                      if e['last_n'] == event['last_n']]
        if len(event_list) > RETRAIN_N:
            self.event_signature_dict[event['signature']].pop(event_list[0][0])


class SigStatsManager():
    """this class handles event data
        add_event(event_object) # adds event to event_signature_dict for signature_name
        get_events(signature) # pulls event array for signature"""
    def __init__(self, event_manager):
        self.stats_list = []
        self.total_std = {}
        self.events = event_manager
        self.best_trades = {}
        # example sig

    def stat_gen(self, sig):
        """execute functions"""
        if len(self.events.get_events(sig['signature'], sig['last_n'])) >= 3:
            price_list, c_dict = self.get_c_dict(sig['signature'], sig['last_n'])
            if c_dict['m_ind'] != 0:
                self.get_total_std(sig['last_n'])
                new_event_list = self.simulate_sl(price_list, c_dict, sig['last_n'])
                self.get_stats(c_dict, new_event_list, sig['signature'], sig['last_n'])
                self.best_times(sig['signature'])
            else:
                pass

    def get_total_std(self, last_n):
        """update stats object with new std value"""
        standard_deviation_sum = 0
        signals = 0
# compile price list
        for sig_id in [sig_id for sig_id in self.stats_list if sig_id['last_n'] == last_n]:
            count = sig_id['count']
            if count > 1:
                # get std
                standard_deviation_sum += sig_id['raw_std'] * count
                signals += count
        if signals > 1:
            self.total_std[last_n] = Decimal((standard_deviation_sum / signals) / STD_MULTIPLIER)
        else:
            self.total_std[last_n] = 1

    def get_c_dict(self, sig, last_n):
        """return c_dict"""
        price_list = []
        std_list = []
# compile event list
        for event_list in self.events.get_events(sig, last_n)[-RETRAIN_N:]:
            price_list.append(event_list['future_ticks'])

        c_dict = {'count': len(price_list)}
# get mean_ind
        numpy_price = numpy.array(price_list)
        price_mean = list(numpy.mean(numpy_price, axis=0))
        if price_mean[-1] > 0:
            c_dict['pattern_side'] = 'bullish'
            c_dict['m_ind'] = price_mean.index(max(price_mean))

        else:
            c_dict['pattern_side'] = 'bearish'
            c_dict['m_ind'] = price_mean.index(min(price_mean))
# get standard_deviation
        for std_ind in price_list:
            std_list.append(std_ind[c_dict['m_ind']])
        c_dict['raw_std'] = stdev(std_list)

        return price_list, c_dict

    def simulate_sl(self, price_list, c_dict, last_n):
        """simulate stop loss by editing price_list"""
        stop_l = 0 - self.total_std[last_n] * SL_L
        break_r = Decimal(self.total_std[last_n]) * BREAK_L
        new_price_list = []
        for price_array in price_list:
            new_price_array = []
            stopped = False
            trigger_price = None

            for ind, tick in enumerate(price_array[1:]):
                if not new_price_array:
                    new_price_array.append(0)
                if not stopped:
                    if c_dict['pattern_side'] == 'bearish':
                        if (tick - price_array[ind - 1]) < -break_r:
                            trigger_price = tick
                            stopped = True
                        elif tick > -stop_l:
                            trigger_price = tick
                            stopped = True
                    else:
                        if (tick - price_array[ind - 1]) > break_r:
                            trigger_price = tick
                            stopped = True
                        elif tick < stop_l:
                            trigger_price = tick
                            stopped = True
                if stopped:
                    new_price_array.append(float(trigger_price))
                else:
                    new_price_array.append(float(tick))
            new_price_list.append(new_price_array)
        return new_price_list

    def get_stats(self, c_dict, new_price_list, sig, last_n):
        """get stats for new event list"""
        new_price_mean = list(numpy.mean(new_price_list, axis=0))
        max_kelly = MAX_KELLY / self.total_std[last_n]
        highers = []
        lowers = []
        for new_price in new_price_list:
            if new_price[c_dict['m_ind']] > 0:
                highers.append(new_price)
            else:
                lowers.append(new_price)
        # compute minimum mean with confidence 95%
        nprice_sd = numpy.std(new_price_list, axis=0)
        new_confidence_interval = Z_VALUE * nprice_sd[c_dict['m_ind']] / sqrt(len(new_price_list))
        if new_price_mean[c_dict['m_ind']] > 0:
            f_profits = new_price_mean[c_dict['m_ind']] - SIM_FEES
            if f_profits < 0:
                f_profits = 0
            mmp = f_profits - new_confidence_interval
            if mmp > 0:
                half_kelly = (mmp / (nprice_sd[c_dict['m_ind']]**2)) / 2
            else:
                half_kelly = 0
        else:
            f_profits = new_price_mean[c_dict['m_ind']] + SIM_FEES
            if f_profits > 0:
                f_profits = 0
            mmp = f_profits + new_confidence_interval
            if mmp < 0:
                half_kelly = abs(mmp / (nprice_sd[c_dict['m_ind']]**2)) / 2
            else:
                half_kelly = 0
        if half_kelly > max_kelly:
            half_kelly = max_kelly
        sharpe = abs((f_profits) / nprice_sd[c_dict['m_ind']] * sqrt((365 * 24) / c_dict['m_ind']))

        for old_list_item in [ind for ind, e in enumerate(self.stats_list)
                              if e['sig'] == sig and e['last_n'] == last_n]:
            self.stats_list.pop(old_list_item)

        self.stats_list.append({'sig': sig,
                                'count': c_dict['count'],
                                'close_on': c_dict['m_ind'],
                                'win_rate': len(highers) / len(new_price_list),
                                'mmp': mmp,
                                'average_gain': f_profits,
                                'confidence': new_confidence_interval,
                                'half_kelly': Decimal(half_kelly),
                                'std': self.total_std[last_n],
                                'raw_std': c_dict['raw_std'],
                                'last_n': last_n,
                                'sharpe': sharpe})

    def best_times(self, sig):
        """evaluate signature over various time frames, select best one"""
        last_ns = [e for e in self.stats_list
                   if e['sig'] == sig]
        last_ns.sort(key=lambda k: k['average_gain'])
        mean_prices = []
        best_n = None
        for index, last_n in enumerate(last_ns[2:-2]):
            mean_prices.append((mean([n['mmp'] for n in last_ns]),
                                index))
        mean_prices.append((last_ns[-1]['mmp'], 0))
        if mean_prices:
            if min([mean_price[0] for mean_price in mean_prices]) > 0:
                best_n = max(mean_prices, key=itemgetter(0))[1]
            elif max([mean_price[0] for mean_price in mean_prices]) < 0:
                best_n = min(mean_prices, key=itemgetter(0))[1]
        if best_n is not None:
            self.best_trades[sig] = last_ns[best_n]
        else:
            if sig in self.best_trades:
                del self.best_trades[sig]


class TickData():
    "this class uploads tick data from cvs and loads it to memory"
    def __init__(self, cvs_file):
        self.cvs_file = cvs_file
        self.price_data = []
        self.duration_dict = {'lines': 0}
        self.daily_tick = {'price': [],
                           'time': [],
                           'pnl': []}
        self.load_cvs()
        self.percent_tick = int(len(self.price_data) / 100)

    def load_cvs(self):
        """load to memory object"""
        if not os.path.isfile(self.cvs_file):
            raise Exception("cvs file doese not exist", "cvs_file")

        with open(self.cvs_file) as backtest_data:
            for line in backtest_data:
                try:
                    close_parse = re.search(r'[0-9]+\.[0-9]+;[0-9]+\.[0-9]+$', line)
                    close_parse = re.search(r'\A[0-9]+\.[0-9]+;', close_parse.group(0))
                    close = Decimal(close_parse.group(0)[:-1])

                    date_parse = re.search(r'^[0-9]+\ [0-9]{4}', line)
                    line_time = datetime.datetime.strptime(date_parse.group(0), '%Y%m%d %H%M')

                except AttributeError:
                    print(line)
                    raise Exception("cvs file will not parse", "cvs_file")

                self.duration_dict['lines'] += 1
                self.price_data.append((close, line_time))
        self.duration()

    def daily_add(self, tick, pnl):
        """add balance to daily list"""
        self.daily_tick['price'].append(tick[0])
        self.daily_tick['time'].append(tick[1])
        self.daily_tick['pnl'].append(pnl)

    def duration(self):
        """updates duration dict"""
        self.duration_dict.update({'first_time': self.price_data[0][1],
                                   'last_time': self.price_data[-1][1],
                                   'duration': self.price_data[-1][1] - self.price_data[0][1]})
