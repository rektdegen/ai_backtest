"""parameters file"""
from decimal import Decimal

EVENT_TABLE_NAME = "historical_sig_stats_3_events"
STATS_TABLE_NAME = "historical_sig_stats_3_stats"
DEFAULT_CVS_FILE = "backtest_data_1000.cvs"
LAST_N_LEN_MULTIPLIER = 3
LAST_N_LEN = 108
LAST_N_RANGE = range(int(LAST_N_LEN / 3), int(LAST_N_LEN * 3), 3)
SEGMENTS = 3
PRICE_COUNT = 2 / 3
PRICE_COUNT = 1
FEES = Decimal(.00075)  # bitmex fees
SLIPPAGE = 0
INITAL_BALANCE = 1
PNL_CALC = "derivatives"

# algo params
SAMPLE_THRESHOLD = 3
SHARPE_THRESHOLD = 5
KELLY_THRESHOLD = 2
WAIT_THRESHOLD = 8760
RETRAIN_N = 12
WAIT_THRESHOLD = 0
SIM_FEES = float(FEES) * 2
FEE_MULTIPLIER = 3
Z_VALUE = 1.96
STD_MULTIPLIER = 1
SL_L = Decimal(1)
BREAK_L = Decimal(.75)
MAX_KELLY = Decimal(.1)  # max drawdown
INIT_STD = Decimal(.02)

SIDES = {'long': True,
         'short': True}
REVERSALS = False

# forex
#FEES = Decimal(.0002 + (0.000154 * 1.5))  # 1broker fees
#SIMLIVE_FEES = float(FEES) * 2
#FEES = 0

#database
DB_NAME = 'backtest_db'
DB_USERNAME = 'user'
DB_PASSWORD = ''

TIME_FRAME = 1

#
LIVE_URL = "https://api.cryptowat.ch/markets/bitstamp/btcusd/ohlc?periods=%s" % (TIME_FRAME * 60)
