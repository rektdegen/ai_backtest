"""here we define algos that execute
design notes:
LamboBot functions
1 siggens
2 event managment
3 stats managament
4 function managment
5 output action info
"""

from parameters import LAST_N_LEN, LAST_N_RANGE, REVERSALS, SEGMENTS
from data_containers import SigEventsManager, SigStatsManager
from algo_position import PositionManager
from algo_database import DataBaseManager
from algo_sigs import SigGen


class LamboBot():
    """this algo compiles a database of signals and evaultates performance of each"""
# returns, buy or sell, and standard_deviation
    def __init__(self, live=False):
        self.data_list = []
        self.live = live
        self.event_manager = SigEventsManager()
        self.stats_manager = SigStatsManager(self.event_manager)
        self.live = live
        self.db_manage = DataBaseManager
        if self.live:
            self.db_manage.connect_to_db(self)
        self.position_manage = PositionManager(self.stats_manager, self.db_manage, live)
        self.sig_gen = SigGen()

    def new_tick(self, tick, position):
        """run every tick"""
        return_data = False
        self.append_tick(tick)
        new_signatures = []
        # get signature
        for last_n in LAST_N_RANGE:
            new_signature = self.get_signature(last_n)
            if new_signature:
                new_signatures.append(new_signature)
            # manage position
            if position and new_signature and REVERSALS:
                self.position_manage.check_reversal(new_signature, position, tick)
            # update event tables
            new_stat = self.event_manager.manage_data(tick, new_signature)
            # update stats tables
            if new_stat:
                self.stats_manager.stat_gen(new_stat)

# return action and action info
        return_data = self.position_manage.return_action(new_signatures)
        return return_data

    def live_tick(self, ticks):
        """run algo live"""
        return_data = False
        new_signatures = []
        self.data_list = ticks
        for last_n in LAST_N_RANGE:
            new_signature = self.get_signature(last_n)
            if new_signature:
                new_signatures.append(new_signature)
        return_data = self.position_manage.return_action(new_signatures)
        print('new_signatures', new_signatures)
        print(len(new_signatures))
        return return_data

    def append_tick(self, new_tick):
        """this appends new tick to tick_list for processing"""
        self.data_list.append(new_tick)
        if len(self.data_list) > LAST_N_LEN * 2:
            self.data_list.pop(0)

    def get_signature(self, last_n):
        """get signatures to check for new sig"""
        signature = self.sig_gen.generate(self.data_list[-last_n:], last_n)
        if self.live:
            if self.repeat_live(signature):
                signature = False
        else:
            if self.repeat(signature):
                signature = False
        return signature

    def repeat_live(self, sig):
        """check for repeat patterns for live data
        this method is less efficient"""
        repeat_sig = False
        event_list = []
        old_signature = {}
        for old_ranges in [range_n for range_n in range(0, sig['last_n'], SEGMENTS)
                           if sig['last_n'] <= len(self.data_list) / 2]:
            old_slice = self.data_list[-old_ranges - sig['last_n'] - 1: -old_ranges - 1]
            old_signature = self.sig_gen.generate(old_slice, sig['last_n'])
            old_signature['range'] = old_ranges
            event_list.append(old_signature)
        for ind, event in enumerate(event_list):

            if sig is not None and event['sig'] == sig['sig']:
                repeat_sig = ind + 1

                break
        event_index = []
        for ind, itm in enumerate(reversed(event_list)):
            if event_index == []:
                event_index.append((itm['sig'], ind))
            elif itm['sig'] not in [tup[0] for tup in event_index]:
                event_index.append((itm['sig'], ind))
        return repeat_sig

    def repeat(self, sig):
        """check for repeat patterns"""
        repeat_sig = False
        for event in [event['signature'] for event in self.event_manager.current_events]:
            if sig is not None and event == sig['sig']:
                repeat_sig = True
                break
        return repeat_sig
