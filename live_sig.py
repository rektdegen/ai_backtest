#!/usr/bin/python3
"""run strategy on live data"""

import json
from datetime import datetime
import requests
from algo import LamboBot
from parameters import TIME_FRAME, LIVE_URL
from time import sleep


class LiveSig():
    """run algo on live"""
    def __init__(self):
        self.lambo_bot = LamboBot()

    def kline2(self):
        """pull live data"""
        print(LIVE_URL)
        ohlc_data = json.loads(requests.get(LIVE_URL).content.decode("utf-8"))
        result = ohlc_data.get('result').get(str(TIME_FRAME * 60))
        self.last_price = result[-1][4]
        return result

    def parse_kline(self, data):
        """parse live data"""
        return_list = []
        for tick in data:
            return_list.append((tick[2], datetime.utcfromtimestamp(tick[0])))
        return return_list

    def live_sig(self):
        """execute algorithm on live data"""
        count = 0
        while True:
            count += 1
            print('iteration:', count)
            try:
                lambo_bot = LamboBot(live=True)
                sig = lambo_bot.live_tick(self.parse_kline(self.kline2()))
                print(sig)
                print('last_price', self.last_price)
                if sig:
                    print('writing sig')
                    self.write_sig(sig)
            except requests.exceptions.ConnectionError:
                print('bad connection... wait and retry')

            print('-----')
            sleep(60)

    def write_sig(self, sig):
        """write signature data to file for transfer to trading bot"""
        print(str(datetime.now()))
        sig['time'] = datetime.now()
        sig = str(sig)
        with open("live.sig", 'w') as f:
            f.write(sig)
        print('string', sig)
        raise 'Finished Test'


if __name__ == '__main__':
    LIVEBOT = LiveSig()
    LIVEBOT.live_sig()
