"""algo scripts for managing positions"""
from decimal import Decimal
from parameters import FEES, FEE_MULTIPLIER, SAMPLE_THRESHOLD, SL_L, BREAK_L, KELLY_THRESHOLD
from parameters import SHARPE_THRESHOLD, SIDES


class ExitRules():
    """this class manages closing conditions"""
    def __init__(self, signal, entry):
        """pass standard deviation, current time and position len"""
        self.sig_expire = signal['close_on']
        self.s_l = Decimal(-signal['std'] * SL_L)
        self.break_r = Decimal(signal['std'] * BREAK_L)
        self.rule_list = [self.trade_expired,
                          self.take_profit,
                          self.stop_l]
        self.entry = entry

    def take_profit(self, contracts, last_tick, tick):
        """set take profit logic"""
        close = False
        if (contracts > 0) and (tick[0] >= last_tick[0] * (self.break_r + 1)):
            close = tick[0]
        elif (contracts < 0) and (tick[0] <= last_tick[0] * (-self.break_r + 1)):
            close = tick[0]
        if close:
            print('entry/exit', self.entry[0], tick[0])
            print('take_profit', (tick[0] / self.entry[0]) - 1, (tick[1] - self.entry[1]))
        return close

    def stop_l(self, contracts, last_tick, tick):
        """set stop loss logic"""
        close = False
        if (contracts > 0) and (tick[0] <= self.entry[0] * (1 + self.s_l)):
            close = self.entry[0] * (1 + self.s_l)
        elif (contracts < 0) and (tick[0] >= self.entry[0] * (1 - self.s_l)):
            close = self.entry[0] * (1 - self.s_l)
        if close:
            print('stop_l entry/exit', self.entry[0], tick[0], (tick[1] - self.entry[1]))
        return close

    def trade_expired(self, contracts, last_tick, tick):
        """set trade experation logic"""
        close = False
        if (tick[1] - last_tick[1]) * self.sig_expire <= tick[1] - self.entry[1]:
            close = tick[0]
        if close:
            print('expired', tick[1] - self.entry[1])
        return close


class PositionManager():
    """return actions and manage positions"""

    def __init__(self, stats_manager, db_manage, live=False):
        self.stats_manager = stats_manager
        self.live = live
        if self.live:
            #self.db_manage = DataBaseManager

            self.db_manage = db_manage
            self.db_manage.connect_to_db(self)

    def db_sig_bests(self, sigs):
        """pull sig data from database"""
        self.db_manage.get_sig_info(sigs)

    def return_action(self, sigs):
        """return buy or sell, and action list
        pull data from stats manager
        ['buy', ['1011001001', 0.81, 1.01025, 4.0, 0.98002]]"""
        sig_info = {}
        action = False
        sig_info_list = []
        for sig in sigs:
            if self.live:
                sig_info = self.db_manage.get_sig_info(self, sig)
                sig_info_list.append(sig_info)
                if any(sig_info):
                    print('sig_info', sig_info)
                    std = sig_info['std']
            elif sig['sig'] in self.stats_manager.best_trades:
                sig_info = dict(self.stats_manager.best_trades[sig['sig']])
                std = self.stats_manager.total_std[sig_info['last_n']]
            if any(sig_info):
                if sig_info['count'] >= SAMPLE_THRESHOLD:
                    both_rules = [sig_info['half_kelly'] >= KELLY_THRESHOLD,
                                  sig_info['sharpe'] >= SHARPE_THRESHOLD]

                    long_rules = [sig_info['mmp'] >= (FEES * FEE_MULTIPLIER),
                                  sig_info['average_gain'] >= FEES * FEE_MULTIPLIER]

                    short_rules = [sig_info['mmp'] <= -(FEES * FEE_MULTIPLIER),
                                   sig_info['average_gain'] <= -(FEES * FEE_MULTIPLIER)]
                if (not action) or (abs(sig_info['mmp']) > abs(action['mmp'])):
                    if all(long_rules) and all(both_rules) and SIDES['long']:
                        action = "buy"
                    elif all(short_rules) and all(both_rules) and SIDES['short']:
                        action = "sell"
                    if action == 'buy' or action == 'sell':
                        print(sig)
                        sig_info['side'] = action
                        if not self.live:
                            self.stats_manager.best_trades[sig['sig']]['std'] = std
                            sig_info['std'] = std
                        action = sig_info
        if self.live and sig_info_list is not None:
            self.best_signature(sig_info_list)
        return action

    def best_signature(self, signatures):
        """return best signatures for information purposes"""
        best_mmp = 0
        best_signature = None
        for signature in signatures:
            if 'mmp' in signature:
                if abs(signature['mmp']) > best_mmp:
                    best_signature = signature
                    best_mmp = abs(signature['mmp'])
        if best_signature:
            print('best_signature:', best_signature['sig'])
            for key in best_signature:
                print(key, best_signature[key])
        else:
            print('all signatures are shit')

    def check_reversal(self, sig, position, tick):
        """disabled pending further research"""
        action = self.return_action(sig)
        if action:
            conditions = [action['side'] == 'buy' and position.contracts < 0,
                          action['side'] == 'sell' and position.contracts > 0]
            print('reversal')
            if any(conditions):
                position.close_position(tick)
