#!/usr/bin/python3
"""this program parses trading data formated in
  line_time,price,volume
into minute candle sticks
with format "20151228 052700;1.097930;1.097930;1.097870;1.097880;0"
time;open;high;low;close;volume

this version does this in a normalized way
all line_times have no lagging seconds and gaps are accounted for
"""

import sys
import os
import re
from datetime import timedelta, datetime
import decimal
# start the script
stick_duration = 60  # 1hr


def round_time(dt=None):
    """
    Round a datetime object down
    to a multiple of a timedelta
    """
    round_to = timedelta(minutes=stick_duration).total_seconds()
    seconds = (dt - dt.min).seconds
    rounding = seconds / round_to

    return dt + timedelta(0, rounding - seconds, -dt.microsecond)


def log_minute(minute_dict):
    """ADD LINE TO CVS FILE"""
    if not os.path.isfile(output):
        action = "w"
    else:
        action = "a"
    with open(output, action) as f:
        f.write(str(minute_dict))
        f.write("\n")


def parse(filename, output):
    """parse input"""
    date = 0
    total_volume = 0
    first_date = None
    clean_date = None
    last_date = None

    minute_volume = 0
    minute_open = 0
    minute_high = 0
    minute_low = 0
    minute_close = 0

    with open(filename) as fileobject:
        for num, line in enumerate(fileobject):
            date_parse = int((re.search('[0-9]+,', line)).group(0)[:-1])
            line_time = datetime.fromtimestamp(date_parse)

            if first_date is None:
                first_date = line_time
            elif first_date < line_time:
                first_date = line_time

            if (clean_date is None) and (first_date is not None):
                clean_date = round_time(first_date)
            if last_date is None:
                last_date = line_time


# devide up into 3 conditions
# 1 minute time is None
# 2 time is less then 1 minute ahead of minute_time
# 3 time is greater then 1 minute ahead of minute_time

            # this condition adds
            while line_time >= clean_date:
                minute_string = '%s;%0.8f;%0.8f;%0.8f;%0.8f;%0.8f' % (clean_date.strftime('%Y%m%d %H%M'),
                                                                      minute_open,
                                                                      minute_high,
                                                                      minute_low,
                                                                      minute_close,
                                                                      minute_volume)
            # "20151228 052700;1.097930;1.097930;1.097870;1.097880;0"
                log_minute(minute_string)
                if minute_volume == 0:
                    print(minute_string)

                minute_volume = 0
                minute_open = minute_close
                minute_high = minute_close
                minute_low = minute_close
                minute_volume = 0
                clean_date += timedelta(minutes=stick_duration)
            minute_open = None

            price_parse = re.search(',[0-9]+\.[0-9]+,', line)
            try:
                price = decimal.Decimal(price_parse.group(0)[:-1][1:])
            except:
                print("\n bad formatting :(")
                print("cannot parse input. exiting... \n")
                exit()
            try:
                volume_parse = re.search(',[0-9]+\.[0-9]+$', line)
                volume = decimal.Decimal(volume_parse.group(0)[1:])
            except:
                print("error on line:", num)
                volume = 0

# append line data to tick entry
            minute_volume += volume

            if minute_open is None:
                minute_open = price

            if minute_high is None:
                minute_high = price
            elif minute_high < price:
                minute_high = price

            if minute_low is None:
                minute_low = price
            elif minute_low > price:
                minute_low = price

            minute_close = price

        time_elapsed = str(first_date - last_date)
        print(time_elapsed + " seconds")


if sys.argv:
    if len(sys.argv) >= 2:
        filename = sys.argv[1]
    else:
        filename = ""
    if len(sys.argv) >= 3:
        output = sys.argv[2]
    else:
        output = "output.cvs"

    if os.path.isfile(output):
        print("\n output.cvs exists. exiting \n")
        exit()

    comand = sys.argv[0]
    if os.path.isfile(filename):
        # if os.path.isfile(output):
        #     print("\n  outputfile already exists :'( \n")
        # else:
        # parse()
            print(filename)

            parse(filename, output)
    else:
        print("\n  please provide a valid file :'( ")
        # return_choices()
